package modbundle

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

const (
	// YML parse yml file
	YML = "YML"
	// JSON parse json file
	JSON = "JSON"
)

// ParseYML parse a YML file
func ParseYML(key, path string) error {
	return parse(key, path, YML)
}

// ParseJSON parse a json file
func ParseJSON(key, path string) error {
	return parse(key, path, JSON)
}

func parse(key, path, format string) error {
	if bundles == nil {
		bundles = make(map[string]*Bundle)
	}
	if _, ok := bundles[key]; ok {
		return fmt.Errorf("you should initialize the `%s` bundle only once", key)
	}

	bytes, err := load(path)
	if err != nil {
		return err
	}

	bundle := new(Bundle)
	bundle.Data = make(map[interface{}]interface{})

	switch format {
	case YML:
		ymlDataMap := make(map[interface{}]interface{})
		yaml.Unmarshal(bytes, ymlDataMap)

		if err := flattenYML(ymlDataMap, "", bundle); err != nil {
			return err
		}
	case JSON:
		jsonDataMap := make(map[string]interface{})
		json.Unmarshal(bytes, &jsonDataMap)

		if err := flattenJSON(jsonDataMap, "", bundle); err != nil {
			return err
		}
	}

	bundles[key] = bundle

	return nil
}

func flattenJSON(children map[string]interface{}, prefix string, b *Bundle) error {
	for i, c := range children {
		if len(prefix) > 0 {
			i = "." + i
		}
		if m, ok := c.(map[string]interface{}); ok {
			flattenJSON(m, prefix+i, b)
		} else {
			d, err := parseEnvVariables(c)
			if err != nil {
				return err
			}
			b.Data[prefix+i] = d
		}
	}

	return nil
}

func flattenYML(children map[interface{}]interface{}, prefix string, b *Bundle) error {
	for i, c := range children {
		n := i.(string)
		if len(prefix) > 0 {
			n = "." + n
		}
		if m, ok := c.(map[interface{}]interface{}); ok {
			flattenYML(m, prefix+n, b)
		} else {
			d, err := parseEnvVariables(c)
			if err != nil {
				return err
			}
			b.Data[prefix+n] = d
		}
	}

	return nil
}

func parseEnvVariables(i interface{}) (interface{}, error) {
	if s, ok := i.(string); ok {
		if strings.HasPrefix(s, "%env=") {
			// replace environment variable indication with an empty string
			s = strings.Replace(s, "%env=", "", 1)
			// split on comma to find default value
			o := split(s, ",")
			// if no default value has been supplied, use empty string
			if len(o) == 1 {
				o = append(o, "")
			}
			// check if environment variable is available
			v, err := env(o[0], o[1])
			// if any error returned, stop the parsing and throw an error
			if err != nil {
				return nil, err
			}
			if len(v) == 0 {
				return nil, fmt.Errorf("environment variable `%s` cannot be empty", s)
			}

			// check if it is an int value
			if n, err := strconv.Atoi(v); err == nil {
				return n, nil
			}

			// check if it is a bool value
			if b, err := strconv.ParseBool(v); err == nil {
				return b, nil
			}

			// check if it is a float value
			if f, err := strconv.ParseFloat(v, 64); err == nil {
				return f, nil
			}

			return v, nil
		}
	}

	return i, nil
}

func split(variable, with string) []string {
	s := strings.Split(variable, with)
	return s
}

// get environment variable or fall back to default value (dv)
func env(variable, dv string) (string, error) {
	v := os.Getenv(variable)
	if len(v) == 0 && len(dv) == 0 {
		return "", fmt.Errorf("environment variable is an empty string and no default value")
	} else if len(v) == 0 && len(dv) > 0 {
		return dv, nil
	}
	return v, nil
}

func load(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return []byte{}, fmt.Errorf("file %s not found", path)
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return []byte{}, err
	}

	return bytes, nil
}
